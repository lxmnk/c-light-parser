class Tag:
    VOID, MAIN, LEFT_PAR, RIGHT_PAR, LEFT_BR, RIGHT_BR, INT, BOOL, END, \
            RET, ASSIGN, ID, LT, GT, FOR, IF, UNKNOWN = range(17)


class Token:
    RESERVED = {'(': Tag.LEFT_PAR,
                ')': Tag.RIGHT_PAR,
                '{': Tag.LEFT_BR,
                '}': Tag.RIGHT_BR,
                ';': Tag.END,
                '=': Tag.ASSIGN,
                '<': Tag.LT,
                '>': Tag.GT,
                'bool': Tag.BOOL,
                'for': Tag.FOR,
                'if': Tag.IF,
                'int': Tag.INT,
                'main': Tag.MAIN,
                'return': Tag.RET,
                'void': Tag.VOID,
                'unknown': Tag.UNKNOWN}

    def __init__(self, tag):
        self.tag = self.RESERVED[tag]

    def __eq__(self, another):
        if self.tag == another.tag:
            return True
        return False


class Number:
    def __init__(self, value):
        self.tag = Tag.INT
        self.value = value

    def __eq__(self, another):
        if self.tag == another.tag and self.value == another.value:
            return True
        return False


class Id:
    def __init__(self, value):
        self.tag = Tag.ID
        self.value = value

    def __eq__(self, another):
        if self.tag == another.tag and self.value == another.value:
            return True
        return False


class SyntaxAnalyzer:
    NO_RET = 'No return type'
    NO_MAIN = 'Here should be `main` function'
    NO_LEFT_PAR = '`(` expected'
    NO_RIGHT_PAR = '`)` expected'
    NO_LEFT_BR = '`{` expected'
    NO_RIGHT_BR = '`}` expected'
    NO_STMT = 'statement expected'
    NO_ID = 'identifier expected'
    NO_NUM = 'number expected'
    NO_END = '`;` expected'
    NO_ASSIGN = '`=` expected'
    NO_NUM_OR_ID = 'number or identifier expected'
    NO_LT_OR_GT = '`<` or `>` expected'
    NO_TYPE = '`bool`, `int` or `void` expected'

    def next_token(self, string):
        self.line = 1
        letters = iter(string)
        for x in letters:
            if x == '\n':
                self.line += 1
            if x.isalpha():
                word = ''
                while x.isalnum():
                    word += x
                    if word in Token.RESERVED:
                        yield Token(word)
                        word = ''
                    try:
                        x = next(letters)
                    except StopIteration:
                        break
                if word:
                    yield Id(word)
            if x.isdigit():
                number = ''
                while x.isdigit():
                    number += x
                    try:
                        x = next(letters)
                    except StopIteration:
                        break
                yield Number(int(number))
            if x in ('(', ')', '{', '}', ';', '=', '>', '<'):
                yield Token(x)
            elif not x.isspace() and not x.isalnum():
                yield Token('unknown')

    def error(self, msg):
        return '{0}: {1}.'.format(self.line, msg)

    def next_tag_in(self, *tags):
        try:
            if next(self.tokens).tag not in tags:
                return False
        except StopIteration:
            return False
        return True

    def check_program(self, program):
        self.tokens = iter(self.next_token(program))
        if not self.next_tag_in(Tag.BOOL, Tag.INT, Tag.VOID):
            return self.error(self.NO_RET)
        if not self.next_tag_in(Tag.MAIN):
            return self.error(self.NO_MAIN)
        if not self.next_tag_in(Tag.LEFT_PAR):
            return self.error(self.NO_LEFT_PAR)
        if not self.next_tag_in(Tag.RIGHT_PAR):
            return self.error(self.NO_RIGHT_PAR)
        if not self.next_tag_in(Tag.LEFT_BR):
            return self.error(self.NO_LEFT_BR)
        return self.check_statement()

    def check_statement(self):
        try:
            current_tag = next(self.tokens).tag
        except StopIteration:
            return self.error(self.NO_STMT)
        if current_tag == Tag.INT:
            error = self.check_declaration()
            if error is not None:
                return error
        elif current_tag == Tag.LEFT_BR:
            error = self.check_statement()
            if error is not None:
                return error
        elif current_tag == Tag.FOR:
            error = self.check_for()
            if error is not None:
                return error
        elif current_tag == Tag.IF:
            error = self.check_if()
            if error is not None:
                return error
        elif current_tag == Tag.RET:
            error = self.check_return()
            if error is not None:
                return error
        if not self.next_tag_in(Tag.RIGHT_BR):
            return self.error(self.NO_RIGHT_BR)

    def check_declaration(self):
        if not self.next_tag_in(Tag.ID):
            return self.error(self.NO_ID)
        if not self.next_tag_in(Tag.ASSIGN):
            return self.error(self.NO_ASSIGN)
        if not self.next_tag_in(Tag.INT):
            return self.error(self.NO_NUM)
        if not self.next_tag_in(Tag.END):
            return self.error(self.NO_END)

    def check_for(self):
        if not self.next_tag_in(Tag.LEFT_PAR):
            return self.error(self.NO_LEFT_PAR)
        if not self.next_tag_in(Tag.BOOL, Tag.INT, Tag.VOID):
            return self.error(self.NO_TYPE)
        error = self.check_declaration()
        if error is not None:
            return error
        error = self.check_bool_expression()
        if error is not None:
            return error
        if not self.next_tag_in(Tag.END):
            return self.error(self.NO_END)
        if not self.next_tag_in(Tag.RIGHT_PAR):
            return self.error(self.NO_RIGHT_PAR)
        if not self.next_tag_in(Tag.LEFT_BR):
            return self.error(self.NO_LEFT_BR)
        error = self.check_statement()
        if error is not None:
            return error

    def check_if(self):
        if not self.next_tag_in(Tag.LEFT_PAR):
            return self.error(self.NO_LEFT_PAR)
        error = self.check_bool_expression()
        if error is not None:
            return error
        if not self.next_tag_in(Tag.RIGHT_PAR):
            return self.error(self.NO_RIGHT_PAR)
        if not self.next_tag_in(Tag.LEFT_BR):
            return self.error(self.NO_LEFT_BR)
        return self.check_statement()

    def check_bool_expression(self):
        if not self.next_tag_in(Tag.INT, Tag.ID):
            return self.error(self.NO_NUM_OR_ID)
        if not self.next_tag_in(Tag.LT, Tag.GT):
            return self.error(self.NO_LT_OR_GT)
        if not self.next_tag_in(Tag.ID):
            return self.error(self.NO_ID)

    def check_return(self):
        if not self.next_tag_in(Tag.INT):
            return self.error(self.NO_NUM)
        if not self.next_tag_in(Tag.END):
            return self.error(self.NO_END)


def main():
    sa = SyntaxAnalyzer()
    program = []
    line = ' '
    print('Input your program. Empty string to break.')
    while line != '':
        line = input()
        program.append(line)
    result = sa.check_program('\n'.join(program))
    if result is None:
        print('Valid.')
    else:
        print(result)


if __name__ == '__main__':
    main()
