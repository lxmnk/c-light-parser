import unittest

from lab4 import Id, Number, SyntaxAnalyzer, Token


class NextTokenTest(unittest.TestCase):
    def setUp(self):
        self.sa = SyntaxAnalyzer()

    def test_one_token(self):
        program = 'void'
        expected = Token(program)
        self.assertEqual(expected, next(self.sa.next_token(program)))

    def test_two_tokens(self):
        program = 'void main'
        expected = [Token('void'), Token('main')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_round_brackets(self):
        program = 'void main()'
        expected = [Token('void'), Token('main'), Token('('), Token(')')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_only_round_brackets(self):
        program = '(()'
        expected = [Token('('), Token('('), Token(')')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_square_brackets(self):
        program = ('void main() {\n'
                   '}            \n')
        expected = [Token('void'), Token('main'), Token('('),
                    Token(')'), Token('{'), Token('}')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_many_brackets(self):
        program = '{)}{({'
        expected = [Token('{'), Token(')'), Token('}'),
                    Token('{'), Token('('), Token('{')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_all_types(self):
        program = '(bool ){void}( int)'
        expected = [Token('('), Token('bool'), Token(')'),
                    Token('{'), Token('void'), Token('}'),
                    Token('('), Token('int'), Token(')')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_semicolon(self):
        program = 'int(; main'
        expected = [Token('int'), Token('('), Token(';'), Token('main')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_number(self):
        program = '131 1290 239 1'
        expected = [Number(131), Number(1290), Number(239), Number(1)]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_return(self):
        program = ('int main() { \n'
                   '    return 0;\n'
                   '}            \n')
        expected = [Token('int'), Token('main'), Token('('),
                    Token(')'), Token('{'), Token('return'),
                    Number(0), Token(';'), Token('}')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_assing(self):
        program = 'a = 1; b=2; c =3; d= 4;'
        expected = [Id('a'), Token('='), Number(1), Token(';'),
                    Id('b'), Token('='), Number(2), Token(';'),
                    Id('c'), Token('='), Number(3), Token(';'),
                    Id('d'), Token('='), Number(4), Token(';')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_bool_expr(self):
        program = 'a > b; c < d; e> f; g<h;'
        expected = [Id('a'), Token('>'), Id('b'), Token(';'),
                    Id('c'), Token('<'), Id('d'), Token(';'),
                    Id('e'), Token('>'), Id('f'), Token(';'),
                    Id('g'), Token('<'), Id('h'), Token(';')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_for_and_if(self):
        program = 'for main if {'
        expected = [Token('for'), Token('main'), Token('if'), Token('{')]
        self.assertEqual(expected, list(self.sa.next_token(program)))

    def test_program_with_if(self):
        program = ('void main() {       \n'
                   '    if (a > b) {    \n'
                   '        int c = 10; \n'
                   '    }               \n'
                   '}                   \n')
        expected = [Token('void'), Token('main'), Token('('), Token(')'),
                    Token('{'), Token('if'), Token('('), Id('a'), Token('>'),
                    Id('b'), Token(')'), Token('{'), Token('int'), Id('c'),
                    Token('='), Number(10), Token(';'), Token('}'), Token('}')]
        self.assertEqual(expected, list(self.sa.next_token(program)))


class CheckTest(unittest.TestCase):
    def setUp(self):
        self.sa = SyntaxAnalyzer()

    def test_empty_program(self):
        program = ''
        expected = '1: {0}.'.format(self.sa.NO_RET)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_wrong_return_type(self):
        program = 'viod'
        expected = '1: {0}.'.format(self.sa.NO_RET)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_no_main(self):
        program = '\nint foo ()'
        expected = '2: {0}.'.format(self.sa.NO_MAIN)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_wrong_main_name(self):
        program = 'int mian ()'
        expected = '1: {0}.'.format(self.sa.NO_MAIN)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_wrong_left_bracket_after_main_token(self):
        program = 'int main)'
        expected = '1: {0}.'.format(self.sa.NO_LEFT_PAR)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_wrong_right_bracket_after_main_token(self):
        program = 'int main(('
        expected = '1: {0}.'.format(self.sa.NO_RIGHT_PAR)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_wrong_left_brace_after_main_token(self):
        program = 'int main() }'
        expected = '1: {0}.'.format(self.sa.NO_LEFT_BR)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_wrong_right_brace_after_main_token(self):
        program = 'int main() {'
        expected = '1: {0}.'.format(self.sa.NO_STMT)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_right_declaration(self):
        program = ('void main() { \n'
                   '    int a = 5;\n'
                   '}             \n')
        self.assertIsNone(self.sa.check_program(program))

    def test_wrong_declaration(self):
        program = ('void main() {  \n'
                   '    int a > 10;\n'
                   '}              \n')
        expected = '2: {0}.'.format(self.sa.NO_ASSIGN)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_right_if(self):
        program = ('int main() {     \n'
                   '    if (5 > a) { \n'
                   '        return 0;\n'
                   '    }            \n'
                   '}                \n')
        self.assertIsNone(self.sa.check_program(program))

    def test_inner_if(self):
        program = ('int main() {         \n'
                   '    if (1 > a) {     \n'
                   '        if (3 < b) { \n'
                   '            return 2;\n'
                   '        }            \n'
                   '    }                \n'
                   '}                    \n')
        self.assertIsNone(self.sa.check_program(program))

    def test_wrong_symbols(self):
        program = 'int $main'
        expected = '1: {0}.'.format(self.sa.NO_MAIN)
        self.assertEqual(expected, self.sa.check_program(program))

    def test_inner_for(self):
        program = ('int main() {                    \n'
                   '    for(int a = 5; 1 < a;) {    \n'
                   '        for(int b = 1; 2 > a;) {\n'
                   '            return 10;          \n'
                   '        }                       \n'
                   '    }                           \n'
                   '}                               \n')
        self.assertIsNone(self.sa.check_program(program))

if __name__ == '__main__':
    unittest.main()
